var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width / 2, 150,
            'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width / 2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // Load all game assets
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.spritesheet('invader1', 'assets/invader132x32x2.png', 32, 32);
        game.load.spritesheet('invader2', 'assets/invader232x32x2.png', 32, 32);
        game.load.spritesheet('invader3', 'assets/invader332x32x2.png', 32, 32);
        game.load.spritesheet('ship', 'assets/player.png', 32, 32);
        game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
        game.load.image('starfield', 'assets/starfield.png');
        game.load.image('ulti', 'assets/ulti.png');
        // Load a new asset that we will use in the menu state
        game.load.image('background', 'assets/background.jpg');
    },
    create: function () {
        // Go to the menu state
        game.state.start('menu');
    }
}; 