var player;
var aliens;
var bullets;
var bulletTime = 0;
var ultiBullets;
var ultiTime = 0;
var ultibool = true;
var cursors;
var fireButton;
var explosions;
var starfield;
var score = 0;
var scoreString = '';
var scoreText;
var ulti = 6;
var ultiString = '';
var ultiText;
var lives;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];
var level = 0;
var playState = {
    create: function () {

        game.physics.startSystem(Phaser.Physics.ARCADE);

        //  The scrolling starfield background
        starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');

        //  Our bullet group
        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(30, 'bullet');
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 1);
        bullets.setAll('outOfBoundsKill', true);
        bullets.setAll('checkWorldBounds', true);

        // ulti

        ultiBullets = game.add.group();
        ultiBullets.enableBody = true;
        ultiBullets.physicsBodyType = Phaser.Physics.ARCADE;
        ultiBullets.createMultiple(30, 'ulti');
        ultiBullets.setAll('anchor.x', 0.5);
        ultiBullets.setAll('anchor.y', 1);
        ultiBullets.setAll('outOfBoundsKill', true);
        ultiBullets.setAll('checkWorldBounds', true);

        // The enemy's bullets
        enemyBullets = game.add.group();
        enemyBullets.enableBody = true;
        enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemyBullets.createMultiple(30, 'enemyBullet');
        enemyBullets.setAll('anchor.x', 0.5);
        enemyBullets.setAll('anchor.y', 1);
        enemyBullets.setAll('outOfBoundsKill', true);
        enemyBullets.setAll('checkWorldBounds', true);

        //  The hero!
        player = game.add.sprite(400, 500, 'ship');
        player.anchor.setTo(0.5, 0.5);
        game.physics.enable(player, Phaser.Physics.ARCADE);
        player.animations.add('fly', [0, 1, 2], 15, true);
        player.play('fly');

        //  The baddies!
        aliens = game.add.group();
        aliens.enableBody = true;
        aliens.physicsBodyType = Phaser.Physics.ARCADE;

        this.createAliens();

        //  The score
        scoreString = 'Score : ';
        scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

        //  Lives
        lives = game.add.group();
        game.add.text(game.world.width - 200, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });
        //  ulti
        ultiString = 'Ulti : ';
        ultiText = game.add.text(game.world.width - 200, 50, ultiString + ulti, { font: '34px Arial', fill: '#fff' });

        //  Text
        stateText = game.add.text(game.world.centerX, game.world.centerY, ' ', { font: '84px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;

        for (var i = 0; i < 3; i++) {
            var ship = lives.create(game.world.width - 80 + (30 * i), 33, 'ship');
            ship.anchor.setTo(0.5, 0.5);
            ship.alpha = 0.4;
        }

        //  An explosion pool
        explosions = game.add.group();
        explosions.createMultiple(30, 'kaboom');
        explosions.forEach(this.setupInvader, this);

        //  And some controls to play the game with
        cursors = game.input.keyboard.createCursorKeys();
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        ultiButton = game.input.keyboard.addKey(Phaser.Keyboard.R);
    },

    createAliens: function () {

        for (var y = 0; y < 4; y++) {
            for (var x = 0; x < 10; x++) {
                if (level == 0) {
                    var alien = aliens.create(x * 48, y * 50, 'invader1');
                } else if (level == 1) {
                    var alien = aliens.create(x * 48, y * 50, 'invader2');
                } else if (level == 2) {
                    var alien = aliens.create(x * 48, y * 50, 'invader3');
                }
                alien.anchor.setTo(0.5, 0.5);
                alien.animations.add('fly', [0, 1], 5, true);
                alien.play('fly');
                alien.body.moves = false;
                alien.live = level;
            }
        }

        aliens.x = 100;
        aliens.y = 50;

        //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
        var tween = game.add.tween(aliens).to({ x: 200 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

        //  When the tween loops it calls descend
        tween.onLoop.add(this.descend, this);
    },

    setupInvader: function (invader) {

        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('kaboom');

    },

    descend: function () {

        aliens.y += 10;

    },

    update: function () {

        //  Scroll the background
        starfield.tilePosition.y += 2;

        if (player.alive) {
            //  Reset the player, then check for movement keys
            player.body.velocity.setTo(0, 0);

            if (cursors.left.isDown) {
                player.body.velocity.x = -200;
            }
            else if (cursors.right.isDown) {
                player.body.velocity.x = 200;
            }
            if (cursors.up.isDown) {
                player.body.velocity.y = -200;
            }
            else if (cursors.down.isDown) {
                player.body.velocity.y = 200;
            }

            //  Firing?
            if (fireButton.isDown) {
                this.fireBullet();
            }

            if(ultiButton.isDown) {
                if(ulti > 0 && ultibool){
                    ulti -= 1;
                    ultibool = false;
                    ultiText.text = ultiString + ulti;
                    this.ultiBullet();
                }
            }else if(ultiButton.isUp){
                ultibool = true;
            }

            if (game.time.now > firingTimer) {
                this.enemyFires();
            }

            //  Run collision
            game.physics.arcade.overlap(bullets, aliens, this.collisionHandler, null, this);
            game.physics.arcade.overlap(ultiBullets, aliens, this.ultiCollisionHandler, null, this);
            game.physics.arcade.overlap(enemyBullets, player, this.enemyHitsPlayer, null, this);
        }

    },

    ultiCollisionHandler: function (bullet, alien) {
        if (alien.live > 0) {
            alien.live -= 1;
        } else {
            //  When a bullet hits an alien we kill them both
            alien.kill();

            //  Increase the score
            score += 20;
            scoreText.text = scoreString + score;

            //  And create an explosion :)
            var explosion = explosions.getFirstExists(false);
            explosion.reset(alien.body.x, alien.body.y);
            explosion.play('kaboom', 30, false, true);
        }

        if (aliens.countLiving() == 0) {
            if (level == 2) {
                score += 1000;
                scoreText.text = scoreString + score;
                game.global.score = score;
                enemyBullets.callAll('kill');
                stateText.text = " You Won \n Click to \n get back to Menu";
                stateText.visible = true;
                level = 0;
                //the "click to restart" handler
                game.input.onTap.addOnce(this.restart, this);
            } else {
                score += 1000;
                scoreText.text = scoreString + score;

                enemyBullets.callAll('kill');
                stateText.text = " Victory, \n Click to level up";
                stateText.visible = true;
                level += 1;    
                //the "click to restart" handler
                game.input.onTap.addOnce(this.levelUp, this);
            }
        }

    },

    collisionHandler: function (bullet, alien) {
        bullet.kill();
        if (alien.live > 0) {
            alien.live -= 1;
        } else {
            //  When a bullet hits an alien we kill them both
            alien.kill();

            //  Increase the score
            score += 20;
            scoreText.text = scoreString + score;

            //  And create an explosion :)
            var explosion = explosions.getFirstExists(false);
            explosion.reset(alien.body.x, alien.body.y);
            explosion.play('kaboom', 30, false, true);
        }

        if (aliens.countLiving() == 0) {
            if (level == 2) {
                score += 1000;
                scoreText.text = scoreString + score;
                game.global.score = score;
                enemyBullets.callAll('kill');
                stateText.text = " You Won \n Click to \n get back to Menu";
                stateText.visible = true;
                level = 0;
                //the "click to restart" handler
                game.input.onTap.addOnce(this.restart, this);
            } else {
                score += 1000;
                scoreText.text = scoreString + score;

                enemyBullets.callAll('kill');
                stateText.text = " Victory, \n Click to level up";
                stateText.visible = true;
                level += 1;    
                //the "click to restart" handler
                game.input.onTap.addOnce(this.levelUp, this);
            }
        }

    },

    enemyHitsPlayer: function (player, bullet) {

        bullet.kill();

        live = lives.getFirstAlive();

        if (live) {
            live.kill();
        }

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('kaboom', 30, false, true);

        // When the player dies
        if (lives.countLiving() < 1) {
            player.kill();
            enemyBullets.callAll('kill');

            stateText.text = " GAME OVER \n Click to restart";
            stateText.visible = true;
            level = 0;

            //the "click to restart" handler
            game.input.onTap.addOnce(this.restart, this);
        }

    },

    enemyFires: function () {

        //  Grab the first bullet we can from the pool
        enemyBullet = enemyBullets.getFirstExists(false);

        livingEnemies.length = 0;

        aliens.forEachAlive(function (alien) {

            // put every living enemy in an array
            livingEnemies.push(alien);
        });


        if (enemyBullet && livingEnemies.length > 0) {

            var random = game.rnd.integerInRange(0, livingEnemies.length - 1);

            // randomly select one of them
            var shooter = livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);

            game.physics.arcade.moveToObject(enemyBullet, player, 120);
            if (level == 0) {
                firingTimer = game.time.now + 1000;
            } else if (level == 1) {
                firingTimer = game.time.now + 750;
            } else {
                firingTimer = game.time.now + 500;
            }
        }

    },

    fireBullet: function () {

        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > bulletTime) {
            //  Grab the first bullet we can from the pool
            bullet = bullets.getFirstExists(false);

            if (bullet) {
                //  And fire it
                bullet.reset(player.x, player.y + 8);
                bullet.body.velocity.y = -400;
                bulletTime = game.time.now + 200;
            }
        }

    },

    ultiBullet: function () {

        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > ultiTime) {
            //  Grab the first bullet we can from the pool
            ultiBullet = ultiBullets.getFirstExists(false);
            if (ultiBullet) {
                //  And fire it
                ultiBullet.reset(player.x, player.y + 8);
                ultiBullet.body.velocity.y = -400;
                ultiTime = game.time.now + 1000;
            }
        }

    },

    resetBullet: function (bullet) {

        //  Called if the bullet goes out of the screen
        bullet.kill();

    },

    restart: function () {

        game.state.start('menu');

    },
    levelUp: function () {

        //  A new level starts
        //resets the life count
        lives.callAll('revive');
        //  And brings the aliens back from the dead :)
        aliens.removeAll();
        this.createAliens();
        //revives the player
        player.revive();
        //hides the text
        stateText.visible = false;

    }
};
